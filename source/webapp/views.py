from django.views.generic import ListView

from webapp.models import Product


class ProductListView(ListView):
    model = Product
    template_name = 'product/product_index.html'
    context_object_name = 'products'