from django.urls import path
from webapp.views import ProductViewList

urlpatterns = [
    path('', ProductViewList.as_view(), name='product_list')
]

app_name = 'webapp'