from django.contrib import admin
from .models import Product


class ProductAdmin(admin.ModelAdmin):
    list_display = ['pk', 'name', 'description', 'category', 'left', 'image', 'price']
    list_filter = ['name', 'category', 'price']
    list_display_links = ['name']
    search_fields = ['name', 'category']
    fields = ['name', 'description', 'category', 'left', 'image', 'price']


admin.site.register(Product, ProductAdmin)
